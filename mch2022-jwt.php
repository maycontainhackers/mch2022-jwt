<?php
/*
 * Plugin Name: REST JWT Auth
 * Description: MCH2021 Sysadmin custom plugin
 * Author: Xesxen
 * Version: 1.0.0
 */

include_once __DIR__ . "/JWT.php";

if (!defined('JWT_AUD')) {
    define('JWT_AUD', '');
}
if (!defined('JWT_PUBKEY')) {
    define('JWT_PUBKEY', '');
}
if (!defined('JWT_ALGO')) {
    define('JWT_ALGO', '');
}

const JWT_HEADER_AUTH = ['authorization'];
const JWT_USER_META_KEY = 'openid-connect-generic-subject-identity';

function wp_validate_jwt( $result ) {
    $headers = array_change_key_case(
        function_exists('apache_request_headers') ? apache_request_headers() : $_SERVER,
        CASE_LOWER
    );

    $token = null;

    foreach ($headers as $name => $header) {
        if (!in_array($name, JWT_HEADER_AUTH)) {
            continue;
        }

        $splits = explode(' ', $header);
        if (count($splits) !== 2 || $splits[0] !== 'Bearer') {
            continue;
        }

        $token = $splits[1];
    }

    if (!$token) {
        return $result;
    }

    $user = jwt_retrieve_user($token);

    if ($user instanceof WP_User) {
        // Explicitly mark current user as not logged-in via cookie
        global $wp_rest_auth_cookie;
        $wp_rest_auth_cookie = false;

        return $user->ID;
    }

    return $result;
}

function jwt_retrieve_user($jwt) {
    $invalid_jwt = new WP_Error(
        'jwt_invalid',
        __( 'Invalid JWT' ),
    );

    if (!JWT_AUD || !JWT_PUBKEY || !JWT_ALGO) {
        return $invalid_jwt;
    }

    try {
        $token = JWT::fromToken($jwt, JWT_PUBKEY, JWT_ALGO);
    } catch (Exception $e) {
        var_dump($e);exit;
    }

    if (!$token || !$token->isValid(JWT_AUD)) {
        return $invalid_jwt;
    }

    /**
     * Validate Keycloak-custom properties existence
     */

    $token_data = $token->getData();

    if (!isset($token_data['email_verified']) || !is_bool($token_data['email_verified']) || !$token_data['email_verified']) {
        return new WP_Error(
            'jwt_email_unverified',
            __( 'Unauthorized' ),
        );
    }

    if (!isset($token_data['roles']) || !is_array($token_data['roles']) || !in_array('access', $token_data['roles'])) {
        return new WP_Error(
            'jwt_no_roles',
            __( 'Unauthorized' ),
        );
    }

    /**
     * Retrieve users
     */

    $users = get_users([
        'meta_key' => JWT_USER_META_KEY,
        'meta_value' => $token_data['sub'],
    ]);

    if (count($users) === 1) {
        return $users[0];
    }

    $user = get_user_by( 'email', $token_data['email'] );

    if ( ! $user ) {
        return new WP_Error(
            'invalid_email',
            __( 'Unknown email address. Check again or try your username.' )
        );
    }

    return $user;
}

add_filter( 'determine_current_user', 'wp_validate_jwt', 20);
